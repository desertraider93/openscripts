apt-get update
apt-get upgrade -y
apt-get install containerd -y
mkdir -p /etc/containerd
sudo su -
containerd config default  /etc/containerd/config.toml
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get install kubeadm kubelet kubectl -y
echo 'net.bridge.bridge-nf-call-iptables = 1' >> /etc/sysctl.conf
sudo -s
echo '1' > /proc/sys/net/ipv4/ip_forward
exit
sudo sysctl --system
sudo modprobe overlay
sudo modprobe br_netfilter
sudo swapoff -a
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml
kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml
kubectl taint nodes --all node-role.kubernetes.io/master-

sudo kubeadm init --pod-network-cidr=192.168.0.0/16 |& tee initsave.txt
